/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\renderer.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Friday, July 5th 2019, 8:07:50 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "common.h"
#include "renderer.h"
#include "image_loader.h"

GLenum glCheckError_(const char *file, int line)
{
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR)
    {
        char *error = malloc(32);
        switch (errorCode)
        {
        case GL_INVALID_ENUM:
            error = "INVALID_ENUM";
            break;
        case GL_INVALID_VALUE:
            error = "INVALID_VALUE";
            break;
        case GL_INVALID_OPERATION:
            error = "INVALID_OPERATION";
            break;
        case GL_STACK_OVERFLOW:
            error = "STACK_OVERFLOW";
            break;
        case GL_STACK_UNDERFLOW:
            error = "STACK_UNDERFLOW";
            break;
        case GL_OUT_OF_MEMORY:
            error = "OUT_OF_MEMORY";
            break;
        }
        printf("%s | %s(%d)\n", error, file, line);
    }
    return errorCode;
}

/* A general OpenGL initialization function.  Sets all of the initial parameters. */
void InitGL(int Width, int Height) // We call this right after our OpenGL window is created.
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // This Will Clear The Background Color To Black
    glClearDepth(1.0);                    // Enables Clearing Of The Depth Buffer
    glDepthFunc(GL_LESS);                 // The Type Of Depth Test To Do
    glEnable(GL_DEPTH_TEST);              // Enables Depth Testing
    glShadeModel(GL_SMOOTH);              // Enables Smooth Color Shading
    glEnable(GL_CULL_FACE);
    glDisable(GL_BLEND);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity(); // Reset The Projection Matrix

    gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window

    glMatrixMode(GL_MODELVIEW);
}

/* The function called when our window is resized (which shouldn't happen, because we're fullscreen) */
void ReSizeGLScene(int Width, int Height)
{
    if (Height == 0) // Prevent A Divide By Zero If The Window Is Too Small
        Height = 1;

    glViewport(0, 0, Width, Height); // Reset The Current Viewport And Perspective Transformation

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
}

// Routine to load an image as a texture.
GLuint RNDR_CreateTextureFromImage(image *img)
{
    
    glGenTextures(1, &(img->id));
    if(img->id){
    glBindTexture(GL_TEXTURE_2D, img->id);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img->width, img->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img->data);
#if 1==2//_arch_dreamcast
    if (img->channels == 3)
    {
        //you'd think GL_RGB but nope
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img->width, img->height, 0, GL_RGB, GL_UNSIGNED_BYTE, img->data);
    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img->width, img->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img->data);
    }
#else
    /*if (img->channels == 3)
    {
        //you'd think GL_RGB but nope
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, img->width, img->height, GL_RGBA, GL_UNSIGNED_BYTE, img->data);
    }
    else
    {
        printf("4\n");
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, img->width, img->height, GL_RGBA, GL_UNSIGNED_BYTE, img->data);
    }*/
#endif
    glCheckError();
    IMG_unload(img);
    }
    glCheckError();
    return img->id;
}