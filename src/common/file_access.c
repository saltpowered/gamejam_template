/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\file_access.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, July 6th 2019, 2:12:12 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "common.h"
#include "file_access.h"

#ifdef _arch_dreamcast
char _path[256];

char *transform_path(char *path)
{
    memset(_path, 0, 256);
    sprintf(_path, "/cd/%s", path);
    return &_path[0];
}
#endif

int Sys_FileLength(FILE *f)
{
    int pos;
    int end;

    pos = ftell(f);
    fseek(f, 0, SEEK_END);
    end = ftell(f);
    fseek(f, pos, SEEK_SET);

    return end;
}