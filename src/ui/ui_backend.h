#ifndef UI_BACKEND_H
#define UI_BACKEND_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\ui\ui_backend.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\ui
 * Created Date: Wednesday, July 31st 2019, 9:00:28 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "common.h"
#include "renderer.h"

void UI_Init(void);
void UI_Set2D(void);
void UI_DrawCharacter(int x, int y, int num);
void UI_DrawString(int x, int y, char *str);
void UI_TextColor(float r, float g, float b);
void UI_DrawPic(int x, int y, image* pic);
void UI_DrawTransPic(int x, int y, image *pic);

#endif /* UI_BACKEND_H */
